import { ScrcpyClient, fetchServer, ScrcpyServerVersion, ScrcpyLogLevel, AndroidCodecLevel, AndroidCodecProfile, ScrcpyScreenOrientation, AndroidMotionEventAction } from './lib/ya-scrcpy-client';
import AdbWebUsbBackend, { AdbWebCredentialStore } from '@yume-chan/adb-backend-webusb';
import { Adb } from '@yume-chan/adb';
import { AppStore } from './App.store';

const CredentialStore = new AdbWebCredentialStore();
const DeviceServerPath = '/data/local/tmp/scrcpy-server.jar';

let scrcpyClient;

export const AdbService = {
    async connect() {
        let backend = (await AdbWebUsbBackend.getDevices())[0];
        if (!backend) {
            backend = await AdbWebUsbBackend.requestDevice()
        }

        if (!backend) {
            throw new Error('No webusb, adb devices connected');
        }

        const adbDevice = new Adb(backend, {});

        console.log('Connecting...');
        await adbDevice.connect(CredentialStore);
        console.log('Connected!');

        console.log('Downloading scrcpy-server');
        let _total;
        const serverBuffer = await fetchServer('assets/scrcpy-server-v1.17', ([downloaded, total]) => {
            _total = total;
            console.log(downloaded, total, Math.round((downloaded / total) * 100));
        });

        console.log('Pushing scrcpy-server to device');
        const sync = await adbDevice.sync();
        await sync.write(
            DeviceServerPath,
            serverBuffer,
            undefined,
            undefined,
            (uploaded) => console.log(`Uploading: ${Math.round((uploaded / _total) * 100)}`)
        );
        console.log('Pushed');

        const options = {
            device: adbDevice,
            path: DeviceServerPath,
            version: ScrcpyServerVersion,
            logLevel: ScrcpyLogLevel.Debug,
            maxFps: 1,
            // maxSize: 2280,
            bitRate: 1000,
            orientation: ScrcpyScreenOrientation.Unlocked,
            tunnelForward: false,
            // TinyH264 only supports Baseline profile
            profile: AndroidCodecProfile.Baseline,
            level: AndroidCodecLevel.Level4,
            encoder: AppStore.get().encoder,
        };

        const client = new ScrcpyClient(options);

        client.onDebug(message => {
            console.debug('[server] ' + message);
        });
        client.onInfo(message => {
            console.log('[server] ' + message);
        });
        client.onError((e) => {
            console.error('[server] ' + e.message);
        });
        client.onClose(() => {
            console.log('SCRCPY Client closed');
        });
        client.onVideoData((e) => {
            // Have to subscribe to this event or the usb connection gets flooded
        })

        await client.start();

        window.scrcpyClient = scrcpyClient = client;
        console.log('SCRCPY Client connected!');
    },
    up(x, y, pointerId) {
        if (!scrcpyClient) return Promise.resolve();

        return scrcpyClient.injectTouch({
            action: AndroidMotionEventAction.Up,
            pointerId,
            pointerX: x,
            pointerY: y,
            pressure: 1 * 65535,
            buttons: 0,
        });
    },
    down(x, y, pointerId) {
        if (!scrcpyClient) return Promise.resolve();

        return scrcpyClient.injectTouch({
            action: AndroidMotionEventAction.Down,
            pointerId,
            pointerX: x,
            pointerY: y,
            pressure: 1 * 65535,
            buttons: 0,
        });
    },
    move(x, y, pointerId) {
        if (!scrcpyClient) return Promise.resolve();

        return scrcpyClient.injectTouch({
            action: AndroidMotionEventAction.Move,
            pointerId,
            pointerX: x,
            pointerY: y,
            pressure: 1 * 65535,
            buttons: 0,
        })
    },
    tap(x, y, pointerId) {
        if (!scrcpyClient) return Promise.resolve();

        return scrcpyClient.injectTouch({
            action: AndroidMotionEventAction.Down,
            pointerId,
            pointerX: x,
            pointerY: y,
            pressure: 1 * 65535,
            buttons: 0,
        }).then(() => scrcpyClient.injectTouch({
            action: AndroidMotionEventAction.Up,
            pointerId,
            pointerX: x,
            pointerY: y,
            pressure: 1 * 65535,
            buttons: 0,
        }))
    },
    back() {
        if (!scrcpyClient) return Promise.resolve();

        return scrcpyClient.pressBackOrTurnOnScreen();
    }
}

// 2280 1080
