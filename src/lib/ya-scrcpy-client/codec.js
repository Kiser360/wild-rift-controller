// See https://developer.android.com/reference/android/media/MediaCodecInfo.CodecProfileLevel
export var AndroidCodecProfile;
(function (AndroidCodecProfile) {
    AndroidCodecProfile[AndroidCodecProfile["Baseline"] = 1] = "Baseline";
    AndroidCodecProfile[AndroidCodecProfile["Main"] = 2] = "Main";
    AndroidCodecProfile[AndroidCodecProfile["Extended"] = 4] = "Extended";
    AndroidCodecProfile[AndroidCodecProfile["High"] = 8] = "High";
    AndroidCodecProfile[AndroidCodecProfile["High10"] = 16] = "High10";
    AndroidCodecProfile[AndroidCodecProfile["High422"] = 32] = "High422";
    AndroidCodecProfile[AndroidCodecProfile["High444"] = 64] = "High444";
    AndroidCodecProfile[AndroidCodecProfile["ConstrainedBaseline"] = 65536] = "ConstrainedBaseline";
    AndroidCodecProfile[AndroidCodecProfile["ConstrainedHigh"] = 524288] = "ConstrainedHigh";
})(AndroidCodecProfile || (AndroidCodecProfile = {}));
export var AndroidCodecLevel;
(function (AndroidCodecLevel) {
    AndroidCodecLevel[AndroidCodecLevel["Level1"] = 1] = "Level1";
    AndroidCodecLevel[AndroidCodecLevel["Level1b"] = 2] = "Level1b";
    AndroidCodecLevel[AndroidCodecLevel["Level11"] = 4] = "Level11";
    AndroidCodecLevel[AndroidCodecLevel["Level12"] = 8] = "Level12";
    AndroidCodecLevel[AndroidCodecLevel["Level13"] = 16] = "Level13";
    AndroidCodecLevel[AndroidCodecLevel["Level2"] = 32] = "Level2";
    AndroidCodecLevel[AndroidCodecLevel["Level21"] = 64] = "Level21";
    AndroidCodecLevel[AndroidCodecLevel["Level22"] = 128] = "Level22";
    AndroidCodecLevel[AndroidCodecLevel["Level3"] = 256] = "Level3";
    AndroidCodecLevel[AndroidCodecLevel["Level31"] = 512] = "Level31";
    AndroidCodecLevel[AndroidCodecLevel["Level32"] = 1024] = "Level32";
    AndroidCodecLevel[AndroidCodecLevel["Level4"] = 2048] = "Level4";
    AndroidCodecLevel[AndroidCodecLevel["Level41"] = 4096] = "Level41";
    AndroidCodecLevel[AndroidCodecLevel["Level42"] = 8192] = "Level42";
    AndroidCodecLevel[AndroidCodecLevel["Level5"] = 16384] = "Level5";
    AndroidCodecLevel[AndroidCodecLevel["Level51"] = 32768] = "Level51";
    AndroidCodecLevel[AndroidCodecLevel["Level52"] = 65536] = "Level52";
    AndroidCodecLevel[AndroidCodecLevel["Level6"] = 131072] = "Level6";
    AndroidCodecLevel[AndroidCodecLevel["Level61"] = 262144] = "Level61";
    AndroidCodecLevel[AndroidCodecLevel["Level62"] = 524288] = "Level62";
})(AndroidCodecLevel || (AndroidCodecLevel = {}));
//# sourceMappingURL=codec.js.map