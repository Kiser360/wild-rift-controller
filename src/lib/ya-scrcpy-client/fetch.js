import { __awaiter } from "tslib";
import { EventEmitter } from "@yume-chan/event";
// import serverUrl from 'file-loader!./scrcpy-server-v1.17';
export const ScrcpyServerVersion = '1.17';
class FetchWithProgress {
    constructor(url) {
        this._downloaded = 0;
        this._total = 0;
        this.progressEvent = new EventEmitter();
        this.promise = this.fetch(url);
    }
    get downloaded() { return this._downloaded; }
    get total() { return this._total; }
    get onProgress() { return this.progressEvent.event; }
    fetch(url) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield window.fetch(url);
            this._total = Number.parseInt((_a = response.headers.get('Content-Length')) !== null && _a !== void 0 ? _a : '0', 10);
            this.progressEvent.fire([this._downloaded, this._total]);
            const reader = response.body.getReader();
            const chunks = [];
            while (true) {
                const result = yield reader.read();
                if (result.done) {
                    break;
                }
                chunks.push(result.value);
                this._downloaded += result.value.byteLength;
                this.progressEvent.fire([this._downloaded, this._total]);
            }
            this._total = chunks.reduce((result, item) => result + item.byteLength, 0);
            const result = new Uint8Array(this._total);
            let position = 0;
            for (const chunk of chunks) {
                result.set(chunk, position);
                position += chunk.byteLength;
            }
            return result.buffer;
        });
    }
}
let cachedValue;
export function fetchServer(serverUrl, onProgress) {
    if (!cachedValue) {
        cachedValue = new FetchWithProgress(serverUrl);
    }
    if (onProgress) {
        cachedValue.onProgress(onProgress);
        onProgress([cachedValue.downloaded, cachedValue.total]);
    }
    return cachedValue.promise;
}
//# sourceMappingURL=fetch.js.map