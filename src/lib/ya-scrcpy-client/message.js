import Struct, { placeholder } from '@yume-chan/struct';
export var ScrcpyControlMessageType;
(function (ScrcpyControlMessageType) {
    ScrcpyControlMessageType[ScrcpyControlMessageType["InjectKeycode"] = 0] = "InjectKeycode";
    ScrcpyControlMessageType[ScrcpyControlMessageType["InjectText"] = 1] = "InjectText";
    ScrcpyControlMessageType[ScrcpyControlMessageType["InjectTouch"] = 2] = "InjectTouch";
    ScrcpyControlMessageType[ScrcpyControlMessageType["InjectScroll"] = 3] = "InjectScroll";
    ScrcpyControlMessageType[ScrcpyControlMessageType["BackOrScreenOn"] = 4] = "BackOrScreenOn";
    ScrcpyControlMessageType[ScrcpyControlMessageType["ExpandNotificationPanel"] = 5] = "ExpandNotificationPanel";
    ScrcpyControlMessageType[ScrcpyControlMessageType["CollapseNotificationPanel"] = 6] = "CollapseNotificationPanel";
    ScrcpyControlMessageType[ScrcpyControlMessageType["GetClipboard"] = 7] = "GetClipboard";
    ScrcpyControlMessageType[ScrcpyControlMessageType["SetClipboard"] = 8] = "SetClipboard";
    ScrcpyControlMessageType[ScrcpyControlMessageType["SetScreenPowerMode"] = 9] = "SetScreenPowerMode";
    ScrcpyControlMessageType[ScrcpyControlMessageType["RotateDevice"] = 10] = "RotateDevice";
})(ScrcpyControlMessageType || (ScrcpyControlMessageType = {}));
export const ScrcpySimpleControlMessage = new Struct()
    .uint8('type', placeholder());
export var AndroidMotionEventAction;
(function (AndroidMotionEventAction) {
    AndroidMotionEventAction[AndroidMotionEventAction["Down"] = 0] = "Down";
    AndroidMotionEventAction[AndroidMotionEventAction["Up"] = 1] = "Up";
    AndroidMotionEventAction[AndroidMotionEventAction["Move"] = 2] = "Move";
    AndroidMotionEventAction[AndroidMotionEventAction["Cancel"] = 3] = "Cancel";
    AndroidMotionEventAction[AndroidMotionEventAction["Outside"] = 4] = "Outside";
    AndroidMotionEventAction[AndroidMotionEventAction["PointerDown"] = 5] = "PointerDown";
    AndroidMotionEventAction[AndroidMotionEventAction["PointerUp"] = 6] = "PointerUp";
    AndroidMotionEventAction[AndroidMotionEventAction["HoverMove"] = 7] = "HoverMove";
    AndroidMotionEventAction[AndroidMotionEventAction["Scroll"] = 8] = "Scroll";
    AndroidMotionEventAction[AndroidMotionEventAction["HoverEnter"] = 9] = "HoverEnter";
    AndroidMotionEventAction[AndroidMotionEventAction["HoverExit"] = 10] = "HoverExit";
    AndroidMotionEventAction[AndroidMotionEventAction["ButtonPress"] = 11] = "ButtonPress";
    AndroidMotionEventAction[AndroidMotionEventAction["ButtonRelease"] = 12] = "ButtonRelease";
})(AndroidMotionEventAction || (AndroidMotionEventAction = {}));
export const ScrcpyInjectTouchControlMessage = new Struct()
    .uint8('type', ScrcpyControlMessageType.InjectTouch)
    .uint8('action', placeholder())
    .uint64('pointerId')
    .uint32('pointerX')
    .uint32('pointerY')
    .uint16('screenWidth')
    .uint16('screenHeight')
    .uint16('pressure')
    .uint32('buttons');
export const ScrcpyInjectTextControlMessage = new Struct()
    .uint8('type', ScrcpyControlMessageType.InjectText)
    .uint32('length')
    .string('text', { lengthField: 'length' });
export var AndroidKeyEventAction;
(function (AndroidKeyEventAction) {
    AndroidKeyEventAction[AndroidKeyEventAction["Down"] = 0] = "Down";
    AndroidKeyEventAction[AndroidKeyEventAction["Up"] = 1] = "Up";
})(AndroidKeyEventAction || (AndroidKeyEventAction = {}));
export var AndroidKeyCode;
(function (AndroidKeyCode) {
    AndroidKeyCode[AndroidKeyCode["Home"] = 3] = "Home";
    AndroidKeyCode[AndroidKeyCode["Back"] = 4] = "Back";
    AndroidKeyCode[AndroidKeyCode["A"] = 29] = "A";
    AndroidKeyCode[AndroidKeyCode["B"] = 30] = "B";
    AndroidKeyCode[AndroidKeyCode["C"] = 31] = "C";
    AndroidKeyCode[AndroidKeyCode["D"] = 32] = "D";
    AndroidKeyCode[AndroidKeyCode["E"] = 33] = "E";
    AndroidKeyCode[AndroidKeyCode["F"] = 34] = "F";
    AndroidKeyCode[AndroidKeyCode["G"] = 35] = "G";
    AndroidKeyCode[AndroidKeyCode["H"] = 36] = "H";
    AndroidKeyCode[AndroidKeyCode["I"] = 37] = "I";
    AndroidKeyCode[AndroidKeyCode["J"] = 38] = "J";
    AndroidKeyCode[AndroidKeyCode["K"] = 39] = "K";
    AndroidKeyCode[AndroidKeyCode["L"] = 40] = "L";
    AndroidKeyCode[AndroidKeyCode["M"] = 41] = "M";
    AndroidKeyCode[AndroidKeyCode["N"] = 42] = "N";
    AndroidKeyCode[AndroidKeyCode["O"] = 43] = "O";
    AndroidKeyCode[AndroidKeyCode["P"] = 44] = "P";
    AndroidKeyCode[AndroidKeyCode["Q"] = 45] = "Q";
    AndroidKeyCode[AndroidKeyCode["R"] = 46] = "R";
    AndroidKeyCode[AndroidKeyCode["S"] = 47] = "S";
    AndroidKeyCode[AndroidKeyCode["T"] = 48] = "T";
    AndroidKeyCode[AndroidKeyCode["U"] = 49] = "U";
    AndroidKeyCode[AndroidKeyCode["V"] = 50] = "V";
    AndroidKeyCode[AndroidKeyCode["W"] = 51] = "W";
    AndroidKeyCode[AndroidKeyCode["X"] = 52] = "X";
    AndroidKeyCode[AndroidKeyCode["Y"] = 53] = "Y";
    AndroidKeyCode[AndroidKeyCode["Z"] = 54] = "Z";
    AndroidKeyCode[AndroidKeyCode["Delete"] = 67] = "Delete";
    AndroidKeyCode[AndroidKeyCode["AppSwitch"] = 187] = "AppSwitch";
})(AndroidKeyCode || (AndroidKeyCode = {}));
export const ScrcpyInjectKeyCodeControlMessage = new Struct()
    .uint8('type', ScrcpyControlMessageType.InjectKeycode)
    .uint8('action', placeholder())
    .uint32('keyCode')
    .uint32('repeat')
    .uint32('metaState');
//# sourceMappingURL=message.js.map