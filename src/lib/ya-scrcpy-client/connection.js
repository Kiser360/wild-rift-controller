import { __awaiter } from "tslib";
import { AdbBufferedStream, EventQueue } from "@yume-chan/adb";
function delay(time) {
    return new Promise(resolve => {
        window.setTimeout(resolve, time);
    });
}
export class ScrcpyClientConnection {
    constructor(device) {
        this.device = device;
    }
    initialize() { }
    dispose() { }
}
export class ScrcpyClientForwardConnection extends ScrcpyClientConnection {
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            return new AdbBufferedStream(yield this.device.createSocket('localabstract:scrcpy'));
        });
    }
    connectAndRetry() {
        return __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < 100; i++) {
                try {
                    return yield this.connect();
                }
                catch (e) {
                    yield delay(100);
                }
            }
            throw new Error(`Can't connect to server after 100 retries`);
        });
    }
    connectAndReadByte() {
        return __awaiter(this, void 0, void 0, function* () {
            const stream = yield this.connectAndRetry();
            // server will write a `0` to signal connection success
            yield stream.read(1);
            return stream;
        });
    }
    getStreams() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                yield this.connectAndReadByte(),
                yield this.connectAndRetry()
            ];
        });
    }
}
export class ScrcpyClientReverseConnection extends ScrcpyClientConnection {
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            // try to unbind first
            try {
                yield this.device.reverse.remove('localabstract:scrcpy');
            }
            catch (_a) {
                // ignore error
            }
            this.streams = new EventQueue();
            this.address = yield this.device.reverse.add('localabstract:scrcpy', 27183, {
                onSocket: (packet, stream) => {
                    this.streams.enqueue(stream);
                },
            });
        });
    }
    accept() {
        return __awaiter(this, void 0, void 0, function* () {
            return new AdbBufferedStream(yield this.streams.dequeue());
        });
    }
    getStreams() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                yield this.accept(),
                yield this.accept(),
            ];
        });
    }
    dispose() {
        // Don't await this!
        // `reverse.remove`'s response will never arrive
        // before we read all pending data from `videoStream`
        this.device.reverse.remove(this.address);
    }
}
//# sourceMappingURL=connection.js.map