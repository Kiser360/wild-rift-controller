import { __awaiter } from "tslib";
import { DataEventEmitter } from '@yume-chan/adb';
import { PromiseResolver } from '@yume-chan/async';
import { EventEmitter } from '@yume-chan/event';
import Struct from '@yume-chan/struct';
import { AndroidCodecLevel, AndroidCodecProfile } from './codec';
import { ScrcpyClientForwardConnection, ScrcpyClientReverseConnection } from "./connection";
import { AndroidKeyEventAction, AndroidMotionEventAction, ScrcpyControlMessageType, ScrcpyInjectKeyCodeControlMessage, ScrcpyInjectTextControlMessage, ScrcpyInjectTouchControlMessage, ScrcpySimpleControlMessage } from './message';
import { parse_sequence_parameter_set } from './sps';
export var ScrcpyLogLevel;
(function (ScrcpyLogLevel) {
    ScrcpyLogLevel["Debug"] = "debug";
    ScrcpyLogLevel["Info"] = "info";
    ScrcpyLogLevel["Warn"] = "warn";
    ScrcpyLogLevel["Error"] = "error";
})(ScrcpyLogLevel || (ScrcpyLogLevel = {}));
class LineReader {
    constructor(text) {
        this.start = 0;
        this.peekEnd = 0;
        this.text = text;
    }
    next() {
        let result = this.peek();
        this.start = this.peekEnd;
        this.peekEnd = 0;
        return result;
    }
    peek() {
        if (this.peekEnd) {
            return this.peekLine;
        }
        const index = this.text.indexOf('\n', this.start);
        if (index === -1) {
            this.peekLine = undefined;
            this.peekEnd = this.text.length;
            return undefined;
        }
        const line = this.text.substring(this.start, index);
        this.peekLine = line;
        this.peekEnd = index + 1;
        return line;
    }
}
function* parseScrcpyOutput(text) {
    const lines = new LineReader(text);
    let line;
    while (line = lines.next()) {
        if (line === '') {
            continue;
        }
        if (line.startsWith('[server] ')) {
            line = line.substring('[server] '.length);
            if (line.startsWith('DEBUG: ')) {
                yield {
                    level: ScrcpyLogLevel.Debug,
                    message: line.substring('DEBUG: '.length),
                };
                continue;
            }
            if (line.startsWith('INFO: ')) {
                yield {
                    level: ScrcpyLogLevel.Info,
                    message: line.substring('INFO: '.length),
                };
                continue;
            }
            if (line.startsWith('ERROR: ')) {
                line = line.substring('ERROR: '.length);
                const message = line;
                let error;
                if (line.startsWith('Exception on thread')) {
                    if (line = lines.next()) {
                        const [errorType, errorMessage] = line.split(': ', 2);
                        const stackTrace = [];
                        while (line = lines.peek()) {
                            if (line.startsWith('\t')) {
                                stackTrace.push(line.trim());
                                lines.next();
                                continue;
                            }
                            break;
                        }
                        error = {
                            type: errorType,
                            message: errorMessage,
                            stackTrace,
                        };
                    }
                }
                yield {
                    level: ScrcpyLogLevel.Error,
                    message,
                    error,
                };
                continue;
            }
        }
        yield {
            level: ScrcpyLogLevel.Info,
            message: line,
        };
    }
}
export var ScrcpyScreenOrientation;
(function (ScrcpyScreenOrientation) {
    ScrcpyScreenOrientation[ScrcpyScreenOrientation["Unlocked"] = -1] = "Unlocked";
    ScrcpyScreenOrientation[ScrcpyScreenOrientation["Portrait"] = 0] = "Portrait";
    ScrcpyScreenOrientation[ScrcpyScreenOrientation["Landscape"] = 1] = "Landscape";
    ScrcpyScreenOrientation[ScrcpyScreenOrientation["PortraitFlipped"] = 2] = "PortraitFlipped";
    ScrcpyScreenOrientation[ScrcpyScreenOrientation["LandscapeFlipped"] = 3] = "LandscapeFlipped";
})(ScrcpyScreenOrientation || (ScrcpyScreenOrientation = {}));
const Size = new Struct()
    .uint16('width')
    .uint16('height');
const VideoPacket = new Struct()
    .int64('pts')
    .uint32('size')
    .arrayBuffer('data', { lengthField: 'size' });
export const NoPts = BigInt(-1);
const ClipboardMessage = new Struct()
    .uint32('length')
    .string('content', { lengthField: 'length' });
const encoderRegex = /^\s+scrcpy --encoder-name '(.*?)'/;
export class ScrcpyClient {
    constructor(options) {
        this.debugEvent = new EventEmitter();
        this.infoEvent = new EventEmitter();
        this.errorEvent = new EventEmitter();
        this.closeEvent = new EventEmitter();
        this._running = false;
        this.sizeChangedEvent = new EventEmitter();
        this.videoDataEvent = new DataEventEmitter();
        this.clipboardChangeEvent = new EventEmitter();
        this.sendingTouchMessage = false;
        this.options = options;
    }
    static getEncoders(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = new ScrcpyClient(Object.assign(Object.assign({}, options), {
                // Provide an invalid encoder name
                // So the server will return all available encoders
                encoder: '_'
            }));
            const resolver = new PromiseResolver();
            const encoders = [];
            client.onError(({ message, error }) => {
                if (error && error.type !== 'com.genymobile.scrcpy.InvalidEncoderException') {
                    resolver.reject(new Error(`${error.type}: ${error.message}`));
                    return;
                }
                const match = message.match(encoderRegex);
                if (match) {
                    encoders.push(match[1]);
                }
            });
            client.onClose(() => {
                resolver.resolve(encoders);
            });
            // Scrcpy server will open connections, before initializing encoder
            // Thus although an invalid encoder name is given, the start process will success
            yield client.start();
            return resolver.promise;
        });
    }
    get backend() { return this.options.device.backend; }
    get onDebug() { return this.debugEvent.event; }
    get onInfo() { return this.infoEvent.event; }
    get onError() { return this.errorEvent.event; }
    get onClose() { return this.closeEvent.event; }
    get running() { return this._running; }
    get screenWidth() { return this._screenWidth; }
    get screenHeight() { return this._screenHeight; }
    get onSizeChanged() { return this.sizeChangedEvent.event; }
    get onVideoData() { return this.videoDataEvent.event; }
    get onClipboardChange() { return this.clipboardChangeEvent.event; }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            const { device, path, version, logLevel = ScrcpyLogLevel.Error, maxSize = 0, bitRate, maxFps = 0, orientation = ScrcpyScreenOrientation.Unlocked, tunnelForward = false, profile = AndroidCodecProfile.Baseline, level = AndroidCodecLevel.Level4, encoder = '-', } = this.options;
            let connection;
            let process;
            try {
                if (tunnelForward) {
                    connection = new ScrcpyClientForwardConnection(device);
                }
                else {
                    connection = new ScrcpyClientReverseConnection(device);
                }
                yield connection.initialize();
                process = yield device.childProcess.spawn([
                    `CLASSPATH=${path}`,
                    'app_process',
                    /*          unused */ '/',
                    'com.genymobile.scrcpy.Server',
                    version,
                    logLevel,
                    maxSize.toString(),
                    bitRate.toString(),
                    maxFps.toString(),
                    orientation.toString(),
                    tunnelForward.toString(),
                    /*            crop */ '-',
                    /* send_frame_meta */ 'true',
                    /*         control */ 'true',
                    /*      display_id */ '0',
                    /*    show_touches */ 'false',
                    /*      stay_awake */ 'true',
                    /*   codec_options */ `profile=${profile},level=${level}`,
                    encoder,
                    // 'OMX.hisi.video.encoder.avc'
                ], {
                    // Disable Shell Protocol to simplify processing
                    shellProtocol: 'disable',
                });
                process.onStdout(this.handleProcessOutput, this);
                const resolver = new PromiseResolver();
                const removeEventListener = process.onExit(() => {
                    resolver.reject('Server died');
                });
                const [videoStream, controlStream] = yield Promise.race([
                    resolver.promise,
                    connection.getStreams(),
                ]);
                removeEventListener();
                this.process = process;
                this.process.onExit(this.handleProcessClosed, this);
                this.videoStream = videoStream;
                this.controlStream = controlStream;
                this._running = true;
                this.receiveVideo();
                this.receiveControl();
            }
            catch (e) {
                yield (process === null || process === void 0 ? void 0 : process.kill());
                throw e;
            }
            finally {
                connection === null || connection === void 0 ? void 0 : connection.dispose();
            }
        });
    }
    handleProcessOutput(data) {
        const string = this.options.device.backend.decodeUtf8(data);
        for (const output of parseScrcpyOutput(string)) {
            switch (output.level) {
                case ScrcpyLogLevel.Debug:
                    this.debugEvent.fire(output.message);
                    break;
                case ScrcpyLogLevel.Info:
                    this.infoEvent.fire(output.message);
                    break;
                case ScrcpyLogLevel.Error:
                    this.errorEvent.fire(output);
                    break;
            }
        }
    }
    handleProcessClosed() {
        this._running = false;
        this.closeEvent.fire();
    }
    receiveVideo() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.videoStream) {
                throw new Error('receiveVideo started before initialization');
            }
            try {
                // Device name, we don't need it
                yield this.videoStream.read(64);
                // Initial video size
                const { width, height } = yield Size.deserialize(this.videoStream);
                this._screenWidth = width;
                this._screenHeight = height;
                this.sizeChangedEvent.fire({
                    width,
                    height,
                    cropLeft: 0,
                    cropRight: 0,
                    cropTop: 0,
                    cropBottom: 0,
                    croppedWidth: width,
                    croppedHeight: height,
                });
                let buffer;
                while (this._running) {
                    const { pts, data } = yield VideoPacket.deserialize(this.videoStream);
                    if (!data || data.byteLength === 0) {
                        continue;
                    }
                    if (pts === NoPts) {
                        const { pic_width_in_mbs_minus1, pic_height_in_map_units_minus1, frame_mbs_only_flag, frame_crop_left_offset, frame_crop_right_offset, frame_crop_top_offset, frame_crop_bottom_offset, } = parse_sequence_parameter_set(data.slice(0));
                        const width = (pic_width_in_mbs_minus1 + 1) * 16;
                        const height = (pic_height_in_map_units_minus1 + 1) * (2 - frame_mbs_only_flag) * 16;
                        const cropLeft = frame_crop_left_offset * 2;
                        const cropRight = frame_crop_right_offset * 2;
                        const cropTop = frame_crop_top_offset * 2;
                        const cropBottom = frame_crop_bottom_offset * 2;
                        const screenWidth = width - cropLeft - cropRight;
                        const screenHeight = height - cropTop - cropBottom;
                        this._screenWidth = screenWidth;
                        this._screenHeight = screenHeight;
                        this.sizeChangedEvent.fire({
                            width,
                            height,
                            cropLeft: cropLeft,
                            cropRight: cropRight,
                            cropTop: cropTop,
                            cropBottom: cropBottom,
                            croppedWidth: screenWidth,
                            croppedHeight: screenHeight,
                        });
                        buffer = data;
                        continue;
                    }
                    let array;
                    if (buffer) {
                        array = new Uint8Array(buffer.byteLength + data.byteLength);
                        array.set(new Uint8Array(buffer));
                        array.set(new Uint8Array(data), buffer.byteLength);
                        buffer = undefined;
                    }
                    else {
                        array = new Uint8Array(data);
                    }
                    yield this.videoDataEvent.fire({
                        pts,
                        size: array.byteLength,
                        data: array.buffer,
                    });
                }
            }
            catch (e) {
                if (!this._running) {
                    return;
                }
            }
        });
    }
    receiveControl() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.controlStream) {
                throw new Error('receiveControl started before initialization');
            }
            try {
                while (true) {
                    const type = yield this.controlStream.read(1);
                    switch (new Uint8Array(type)[0]) {
                        case 0:
                            const { content } = yield ClipboardMessage.deserialize(this.controlStream);
                            this.clipboardChangeEvent.fire(content);
                            break;
                        default:
                            throw new Error('unknown control message type');
                    }
                }
            }
            catch (e) {
                if (!this._running) {
                    return;
                }
            }
        });
    }
    injectKeyCode(message) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.controlStream) {
                throw new Error('injectKeyCode called before initialization');
            }
            yield this.controlStream.write(ScrcpyInjectKeyCodeControlMessage.serialize(Object.assign(Object.assign({}, message), { type: ScrcpyControlMessageType.InjectKeycode, action: AndroidKeyEventAction.Down }), this.backend));
            yield this.controlStream.write(ScrcpyInjectKeyCodeControlMessage.serialize(Object.assign(Object.assign({}, message), { type: ScrcpyControlMessageType.InjectKeycode, action: AndroidKeyEventAction.Up }), this.backend));
        });
    }
    injectText(text) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.controlStream) {
                throw new Error('injectText called before initialization');
            }
            yield this.controlStream.write(ScrcpyInjectTextControlMessage.serialize({
                type: ScrcpyControlMessageType.InjectText,
                text,
            }, this.backend));
        });
    }
    injectTouch(message) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.controlStream) {
                throw new Error('injectTouch called before initialization');
            }

            if (!this.screenWidth || !this.screenHeight) {
                return;
            }
            // ADB streams are actually pretty low-bandwidth and laggy
            // Re-sample move events to avoid flooding the connection
            if (this.sendingTouchMessage &&
                message.action === AndroidMotionEventAction.Move) {
                return;
            }
            this.sendingTouchMessage = true;
            const buffer = ScrcpyInjectTouchControlMessage.serialize(Object.assign(Object.assign({}, message), { type: ScrcpyControlMessageType.InjectTouch, screenWidth: this.screenWidth, screenHeight: this.screenHeight }), this.backend);
            yield this.controlStream.write(buffer);
            this.sendingTouchMessage = false;
        });
    }
    pressBackOrTurnOnScreen() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.controlStream) {
                throw new Error('pressBackOrTurnOnScreen called before initialization');
            }
            const buffer = ScrcpySimpleControlMessage.serialize({ type: ScrcpyControlMessageType.BackOrScreenOn }, this.backend);
            yield this.controlStream.write(buffer);
        });
    }
    close() {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._running) {
                return;
            }
            this._running = false;
            (_a = this.videoStream) === null || _a === void 0 ? void 0 : _a.close();
            (_b = this.controlStream) === null || _b === void 0 ? void 0 : _b.close();
            yield ((_c = this.process) === null || _c === void 0 ? void 0 : _c.kill());
        });
    }
}
//# sourceMappingURL=client.js.map