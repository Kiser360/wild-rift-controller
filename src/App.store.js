const { Store } = require('svelte/store');

export const AppStore = window.store = new Store();

const defaultControls = {
    ATK: [2030, 860],
    ATK_1: [1900, 1020],
    ATK_2: [2140, 675],
    ABILITY_Q: [1730, 930],
    ABILITY_W: [1780, 800],
    ABILITY_E: [1880, 620],
    ABILITY_R: [2040, 570],
    SUM_1: [1400, 950],
    SUM_2: [1520, 950],
    ENCHANTMENT: [1600, 815],
    WARD: [2020, 370],
    RECALL: [860, 950],
    SHOP_1: [180, 440],
    SHOP_2: [180, 600],
    SCOREBOARD: [2070, 35],
    LEVEL_UP: {
        ABILITY_Q: [1640, 820],
        ABILITY_W: [1670, 670],
        ABILITY_E: [1790, 530],
        ABILITY_R: [1950, 450],
    }
};

const storedControls = localStorage.getItem('controls');
const storedJoystick = localStorage.getItem('joystick');
const storedEncoder = localStorage.getItem('encoder');

AppStore.set({
    Controls: storedControls ? JSON.parse(storedControls) : defaultControls,
    Joystick: storedJoystick ? JSON.parse(storedJoystick) : {
        origin: [340, 775],
        scale: 150,
    },
    encoder: storedEncoder || 'c2.qti.avc.encoder',
});

AppStore.on('update', ({ current, changed }) => {
    if (changed.Controls) {
        localStorage.setItem('controls', JSON.stringify(current.Controls))
    }
    if (changed.Joystick) {
        localStorage.setItem('joystick', JSON.stringify(current.Joystick))
    }
    if (changed.encoder) {
        localStorage.setItem('encoder', current.encoder);
    }

});
