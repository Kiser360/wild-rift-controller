import App from './App.html';
import { AppStore } from './App.store';
import { AdbService } from './adb.service';
import { gamepad$ } from './gamepad.service';
import { first, switchMap, map, share, filter } from 'rxjs/operators';

let Controls = AppStore.get().Controls;

AppStore.on('update', ({ current, changed }) => {
	if (!changed.Controls) return;

	Controls = current.Controls;
});

const PS4_Buttons = {
	'CROSS': 0,
	'CIRCLE': 1,
	'SQUARE': 2,
	'TRIANGLE': 3,
	'L1': 4,
	'R1': 5,
	'L2': 6,
	'R2': 7,
	'SHARE': 8,
	'OPTIONS': 9,
	'L3': 10,
	'R3': 11,
	'D_UP': 12,
	'D_DOWN': 13,
	'D_LEFT': 14,
	'D_RIGHT': 15,
	'HOME': 16,
	'TOUCHPAD': 17,
}

const Tapables = new Map([
	[PS4_Buttons.D_LEFT, 'SUM_1'],
	[PS4_Buttons.D_RIGHT, 'SUM_2'],
	[PS4_Buttons.D_DOWN, 'ENCHANTMENT'],
	[PS4_Buttons.HOME, 'RECALL'],
	[PS4_Buttons.SHARE, 'WARD'],
	[PS4_Buttons.L3, 'SHOP_1'],
	[PS4_Buttons.R3, 'SHOP_2'],
]);

const Attacks = new Map([
	[PS4_Buttons.CROSS, 'ATK'],
	[PS4_Buttons.SQUARE, 'ATK_1'],
	[PS4_Buttons.CIRCLE, 'ATK_2'],
]);

const Abilities = new Map([
	[PS4_Buttons.L1, 'ABILITY_Q'],
	[PS4_Buttons.R1, 'ABILITY_W'],
	[PS4_Buttons.L2, 'ABILITY_E'],
	[PS4_Buttons.R2, 'ABILITY_R'],
]);

const abilityButtons = [PS4_Buttons.R1, PS4_Buttons.L1, PS4_Buttons.R2, PS4_Buttons.L2];

// const gamepad$ = new Subject();
const connectedGamepad$ = gamepad$.pipe(
	first(),
	switchMap(() => AdbService.connect()),
	switchMap(() => gamepad$),
	share(),
	// Add some extra computed state
	map((s) => ({
		...s,
		isLevelingUp: s.pressedButtons.concat(s.heldButtons).includes(PS4_Buttons.D_UP),
		releasedAndPressedButtons: s.pressedButtons.concat(s.releasedButtons),
		containsAbilityButton: s.pressedButtons.concat(s.releasedButtons).some(b => abilityButtons.includes(b))
	}))
);

let isVirtualJoystickDown = false;
const joystickId = BigInt(Math.floor(Math.random() * 65535));

let Joystick = AppStore.get().Joystick;
let origin = {
	x: Joystick.origin[0],
	y: Joystick.origin[1],
};
let scale = Joystick.scale;

AppStore.on('update', ({ current, changed }) => {
	if (!changed.Joystick) return;

	origin = {
		x: current.Joystick.origin[0],
		y: current.Joystick.origin[1],
	};
	scale = current.Joystick.scale;
});

(function handleMove() {
	connectedGamepad$.subscribe(({ leftStick }) => {
		// Starting joystick
		if (leftStick && !isVirtualJoystickDown) {
			isVirtualJoystickDown = true;
			AdbService.down(origin.x, origin.y, joystickId)
				.then(() => AdbService.move(origin.x + (scale * leftStick.x), origin.y + (scale * leftStick.y), joystickId));
		}
		// Ending Joystick
		else if (!leftStick && isVirtualJoystickDown) {
			AdbService.move(origin.x, origin.y, joystickId).then(() => {
				AdbService.up(origin.x, origin.y, joystickId);
				isVirtualJoystickDown = false;
			});
		}
		// Move joystick
		else if (leftStick) {
			AdbService.move(origin.x + (scale * leftStick.x), origin.y + (scale * leftStick.y), joystickId);
		}

	});
})();

(function handleScoreboard() {
	connectedGamepad$.subscribe(({ pressedButtons, releasedButtons }) => {
		if (pressedButtons.includes(PS4_Buttons.OPTIONS)) {
			AdbService.tap(...Controls.SCOREBOARD, BigInt(PS4_Buttons.OPTIONS));
		}

		if (releasedButtons.includes(PS4_Buttons.OPTIONS)) {
			AdbService.back();
		}
	})
})();

(function handleAbilities() {
	connectedGamepad$.pipe(
		filter(({ isLevelingUp, containsAbilityButton }) => containsAbilityButton && !isLevelingUp),
	).subscribe(({ pressedButtons, releasedButtons }) => {
		pressedButtons
			.filter(b => abilityButtons.includes(b))
			.forEach(b => AdbService.down(...Controls[Abilities.get(b)], BigInt(b)));

		releasedButtons
			.filter(b => abilityButtons.includes(b))
			.forEach(b => AdbService.up(...Controls[Abilities.get(b)], BigInt(b)));
	})
})();

(function handleAbilityLevelUp() {
	connectedGamepad$.pipe(
		filter(({ isLevelingUp, containsAbilityButton }) => containsAbilityButton && isLevelingUp)
	).subscribe(({ releasedButtons }) => {
		const releasedAbilityButtons = releasedButtons.filter(b => abilityButtons.includes(b));

		if (releasedAbilityButtons.length) {
			const firstAbilityButton = releasedAbilityButtons[0];
			const coords = Controls.LEVEL_UP[Abilities.get(firstAbilityButton)];
			AdbService.tap(...coords, BigInt(firstAbilityButton));
		}
	});
})();

const tapableButtons = new Set(Tapables.keys());
(function handleTapables() {
	connectedGamepad$.subscribe(({ pressedButtons, releasedButtons }) => {
		pressedButtons
			.filter(b => tapableButtons.has(b))
			.forEach(b => AdbService.down(...Controls[Tapables.get(b)], BigInt(b)));

		releasedButtons
			.filter(b => tapableButtons.has(b))
			.forEach(b => AdbService.up(...Controls[Tapables.get(b)], BigInt(b)));
	})
})();

const atkPointerId = 9001;
const abilitiesOrTapables = new Set(abilityButtons.concat(Array.from(Tapables.keys())));
(function handleAttacks() {
	let heldAttackButton = null;
	const attackButtons = [PS4_Buttons.CROSS, PS4_Buttons.SQUARE, PS4_Buttons.CIRCLE];

	connectedGamepad$.subscribe(async ({ pressedButtons, heldButtons, releasedButtons }) => {
		const allButtonsEvents = new Set(pressedButtons.concat(heldButtons).concat(releasedButtons));
		const heldAndPressedButtons = heldButtons.concat(pressedButtons);
		const isAttacking = heldAttackButton !== null;

		// Do not handle attack buttons if casting a tapable
		if (Array.from(allButtonsEvents).some(b => abilitiesOrTapables.has(b))) {

			// Release any held attack buttons
			if (isAttacking) {
				AdbService.up(...Controls[Attacks.get(heldAttackButton)], BigInt(atkPointerId));
				heldAttackButton = null;
			}
		}

		// If holding or pressing an attack button
		else if (heldAndPressedButtons.some(b => attackButtons.includes(b))) {
			const highestPriorityAttackButton = attackButtons.filter(b => heldAndPressedButtons.includes(b))[0];

			// If not the button already being held
			if (heldAttackButton !== highestPriorityAttackButton) {

				// Release and press the different attack button
				if (isAttacking) {
					await AdbService.up(...Controls[Attacks.get(heldAttackButton)], BigInt(atkPointerId));
				}

				heldAttackButton = highestPriorityAttackButton;
				AdbService.down(...Controls[Attacks.get(heldAttackButton)], BigInt(atkPointerId));
			}
		}
		// No attack buttons are pressed/held, attack button was just released and have an attack button previously held
		else if (releasedButtons.some(b => attackButtons.includes(b)) && isAttacking) {
			AdbService.up(...Controls[Attacks.get(heldAttackButton)], BigInt(atkPointerId));
			heldAttackButton = null;
		}

	});
})();

const app = new App({
	target: document.body,
});

export default app;

// Hold R1, R2, L1, L2 Will allow ability aiming, flick R2 to fire in direction R3 to cancel
// Aim with right stick at anytime, release R1, R2 L1, L2 to fire, hold to preview action R3 to cancel
// L3 + R3 to Open Shop (Long Term capability)
// TRIANGLE Cycle through targets, first tap starts and 1st target, taps after within threshold cycle through targets (???)
// SHARE Place ward, right-stick to aim, release share to place
// HOLD touchpad and RIGHT-STICK (either stick?) to navigate map, R3 to autopath
// ? L3 to flash in direction

