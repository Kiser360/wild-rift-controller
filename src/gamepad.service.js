import { fromEvent, timer } from "rxjs";
import { pairwise, share, take, switchMap, map, distinctUntilChanged } from "rxjs/operators";
import isEqual from 'lodash.isequal';

const freq = 1000 / 30;
const deadZoneRadius = .2;

export const gamepad$ = fromEvent(window, 'gamepadconnected').pipe(
    take(1),
    switchMap(() => timer(0, freq)),
    map(() => navigator.getGamepads()[0]),
    pairwise(),
    map(([prev, curr]) => {
        const prevUnpressedButtonIds = prev.buttons.map((b, i) => ({ pressed: b.pressed, id: i })).filter(b => !b.pressed).map(b => b.id);
        const currPressedButtonIds = curr.buttons.map((b, i) => ({ pressed: b.pressed, id: i })).filter(b => b.pressed).map(b => b.id);

        const currUnpressedButtonIds = curr.buttons.map((b, i) => ({ pressed: b.pressed, id: i })).filter(b => !b.pressed).map(b => b.id);
        const prevPressedButtonIds = prev.buttons.map((b, i) => ({ pressed: b.pressed, id: i })).filter(b => b.pressed).map(b => b.id);

        const pressedButtons = currPressedButtonIds.filter(id => prevUnpressedButtonIds.includes(id));
        const releasedButtons = prevPressedButtonIds.filter(id => currUnpressedButtonIds.includes(id));
        const heldButtons = currPressedButtonIds.filter(id => prevPressedButtonIds.includes(id));

        const x = curr.axes[0];
        const y = curr.axes[1];
        const dist = Math.abs(Math.sqrt((x * x) + (y * y)));
        const leftStickDead = dist < deadZoneRadius;

        return {
            // ...curr,
            pressedButtons,
            releasedButtons,
            heldButtons,
            leftStick: !leftStickDead ? { x, y } : undefined,
        }
    }),
    distinctUntilChanged(isEqual),
    share(),
);
fromEvent(window, 'gamepadconnected').pipe(
    take(1)
).subscribe(() => {
    const { vibrationActuator } = navigator.getGamepads()[0];

    vibrationActuator.playEffect(vibrationActuator.type, {
        startDelay: 0,
        duration: 350,
        weakMagnitude: 1,
        strongMagnitude: .25,
    })
});

// Make it hot and keep it alive 4eva
gamepad$.subscribe(() => { });
